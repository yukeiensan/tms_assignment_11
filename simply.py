import sys
import argparse
from com.print_screens import screens
from com.core.simply_core import (create_connection, get_portfolio_value, get_cash_balance, deposit_screen,
                                  enter_prices, get_todays_activity, trade_screen)
from com.core.eod_lib import run_eod
from com.core.portfolio_libs import portfolio_screen
from com.graphical.display import Trader
from PySide2.QtWidgets import QApplication


# Global Variables
CONFIG = {
    'db_location': "db/trade.db",
    'report_location': 'reports/report.csv',
    'cash_validation': False,
    'is_prod': True
}


def terminal_rendering():
    conn = create_connection(CONFIG)
    is_running = True

    with conn:
        while is_running:
            screens.clear_screen()
            portfolio_value = get_portfolio_value(conn)
            cash_balance = get_cash_balance(conn)
            screens.print_banner("Simply Trade v1.1")
            print("//!\\\\ For optimal experience mind using --graphical for graphical rendering instead //!\\\\\n\n")
            screens.print_options_screen(cash_balance, portfolio_value)
            print("")
            print("-------------------------------")
            ans = input(">> ")
            if ans == "1":
                trade_screen(conn, CONFIG)
            elif ans == "2":
                get_todays_activity(conn, CONFIG)
            elif ans == "3":
                deposit_screen(conn)
            elif ans == "4":
                portfolio_screen(conn)
            elif ans == "5":
                enter_prices(conn)
            elif ans == "6":
                run_eod(conn, CONFIG)
            elif ans == "7":
                is_running = False


def graphical_rendering():
    app = QApplication(sys.argv)

    widget = Trader(CONFIG)
    widget.resize(784, 300)
    widget.show()

    app.exec_()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--cash_validation")
    parser.add_argument("--is_prod")
    parser.add_argument("--graphical", help="Run the program in graphical mode", action="store_true")
    args = vars(parser.parse_args(sys.argv[1:]))
    if args['cash_validation'] is not None:
        CONFIG['cash_validation'] = args['cash_validation']
    if args['is_prod'] is not None:
        CONFIG['is_prod'] = args['is_prod']

    if args["graphical"]:
        CONFIG['is_prod'] = False
        graphical_rendering()
    else:
        terminal_rendering()




