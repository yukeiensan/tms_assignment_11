from com.core.portfolio_libs import get_portfolio, get_portfolio_value
from com.core.price_lib import print_open_prices, get_last_price_type_date_id_by_ticker
from com.core.eod_lib import eod_date_validation, get_list_of_open_positions_for_eod, set_eod_for_ticker
from com.core.simply_core import (create_connection, get_cash_balance, deposit, transaction, ActionType,
                                  get_transactions_by_date)
from PySide2.QtWidgets import (QLabel, QPushButton, QVBoxLayout, QWidget, QSpinBox, QDoubleSpinBox, QComboBox,
                               QLineEdit, QErrorMessage, QTableWidget)
from PySide2.QtCore import Slot, Qt
from datetime import datetime


class Trader(QWidget):
    def __init__(self, config):
        QWidget.__init__(self)
        self.widgets = {
            "header":       {},
            "default":      {},
            "trade":        {},
            "activity":     {},
            "deposit":      {},
            "portfolio":    {},
            "price":        {},
            "eod":          {},
            "credits":      {}
        }

        self.config = config
        self.database = create_connection(config)
        self.setWindowTitle("Simply Trade v1.1")

        self.error_dialog = QErrorMessage()

        # Values
        self.widgets["header"]["portfolio_value_label"] = QLabel()
        self.widgets["header"]["cash_balance_value_label"] = QLabel()
        self.widgets["header"]["debt_label"] = QLabel()
        self.widgets["header"]["debt_to_go"] = QLabel()
        self.portfolio_value = 0
        self.cash_balance_value = 0
        self.debt_value = 0
        self.debt_percentage = 0

        self.update_infos()
        self.define_menu_widgets()
        self.define_trade_widgets()
        self.define_activity_widgets()
        self.define_deposit_widgets()
        self.define_portfolio_widgets()
        self.define_price_widgets()
        self.define_eod_widgets()
        self.define_credits_widget()

        # Toggle view
        self.toggle = "default"
        self.old_toggle = None

        self.layout = QVBoxLayout()
        self.link_widgets_to_layout()
        self.toggle_layout()
        self.setLayout(self.layout)

        self.define_button_events()

    def define_menu_widgets(self):
        self.widgets["default"]["buttons_label"] = QLabel("Available actions:")
        self.widgets["default"]["trade_button"] = QPushButton("Trade")
        self.widgets["default"]["activity_button"] = QPushButton("Activity")
        self.widgets["default"]["deposit_button"] = QPushButton("Deposit")
        self.widgets["default"]["portfolio_button"] = QPushButton("Portfolio")
        self.widgets["default"]["price_button"] = QPushButton("Prices")
        self.widgets["default"]["eod_button"] = QPushButton("EOD")
        self.widgets["default"]["credits_button"] = QPushButton("Credits")

    def define_trade_widgets(self):
        self.widgets["trade"]["action_list"] = QComboBox()
        self.widgets["trade"]["action_list"].addItem("Buy")
        self.widgets["trade"]["action_list"].addItem("Sell")
        self.widgets["trade"]["share_label"] = QLabel("Ticker symbol:")
        self.widgets["trade"]["share_input"] = QLineEdit()
        self.widgets["trade"]["share_amount_label"] = QLabel("Shares amount:")
        self.widgets["trade"]["share_amount_input"] = QSpinBox()
        self.widgets["trade"]["share_amount_input"].setMaximum(999999999)
        self.widgets["trade"]["share_price_label"] = QLabel("Unit price:")
        self.widgets["trade"]["share_price_input"] = QDoubleSpinBox()
        self.widgets["trade"]["share_price_input"].setMaximum(999999999.99)
        self.widgets["trade"]["button_validate"] = QPushButton("Ok")
        self.widgets["trade"]["button_cancel"] = QPushButton("Cancel")

    def define_activity_widgets(self):
        table = QTableWidget()
        table.setRowCount(1)
        table.setColumnCount(7)
        table.setCellWidget(0, 0, QLabel("Id"))
        table.setCellWidget(0, 1, QLabel("Type"))
        table.setCellWidget(0, 2, QLabel("Amount"))
        table.setCellWidget(0, 3, QLabel("Date"))
        table.setCellWidget(0, 4, QLabel("Ticker"))
        table.setCellWidget(0, 5, QLabel("Shares"))
        table.setCellWidget(0, 6, QLabel("Price"))
        self.widgets["activity"]["table"] = table
        self.widgets["activity"]["button_return"] = QPushButton("Return")

    def update_activity_data(self):
        data = get_transactions_by_date(self.database)
        table = self.widgets["activity"]["table"]
        table.setRowCount(len(data) + 1)
        for index, row in enumerate(data):
            table.setCellWidget(index + 1, 0, QLabel(str(row[0])))
            table.setCellWidget(index + 1, 1, QLabel(str(row[1])))
            table.setCellWidget(index + 1, 2, QLabel(str(row[3])))
            table.setCellWidget(index + 1, 3, QLabel(str(row[4])))
            table.setCellWidget(index + 1, 4, QLabel(str(row[8])))
            table.setCellWidget(index + 1, 5, QLabel(str(row[5])))
            table.setCellWidget(index + 1, 6, QLabel(str(row[6])))

        self.widgets["activity"]["table"] = table

    def define_deposit_widgets(self):
        self.widgets["deposit"]["label"] = QLabel("How much do you want to deposit?")
        self.widgets["deposit"]["input"] = QDoubleSpinBox()
        self.widgets["deposit"]["input"].setMaximum(9999999999.99)
        self.widgets["deposit"]["button_validate"] = QPushButton("Ok")
        self.widgets["deposit"]["button_cancel"] = QPushButton("Cancel")

    def define_portfolio_widgets(self):
        table = QTableWidget()
        table.setRowCount(1)
        table.setColumnCount(6)
        table.setCellWidget(0, 0, QLabel("Ticker"))
        table.setCellWidget(0, 1, QLabel("Shares"))
        table.setCellWidget(0, 2, QLabel("Price"))
        table.setCellWidget(0, 3, QLabel("Market Value"))
        table.setCellWidget(0, 4, QLabel("Change"))
        table.setCellWidget(0, 5, QLabel("Percent Change"))
        self.widgets["portfolio"]["table"] = table
        self.widgets["portfolio"]["button_return"] = QPushButton("Return")

    def update_portfolio_data(self):
        data = get_portfolio(self.database)
        table = self.widgets["portfolio"]["table"]
        table.setRowCount(len(data) + 1)
        for index, row in enumerate(data):
            if row['price_prior'] != 0:
                percentage_change = round(((row['price'] / row['price_prior']) - 1) * 100, 2)
            else:
                percentage_change = 0
            change = round(row['price'] - row['price_prior'], 2)
            table.setCellWidget(index + 1, 0, QLabel(str(row["ticker"])))
            table.setCellWidget(index + 1, 1, QLabel(str(row["shares"])))
            table.setCellWidget(index + 1, 2, QLabel(str(row["price"])))
            table.setCellWidget(index + 1, 3, QLabel(str(row["market_value"])))
            table.setCellWidget(index + 1, 4, QLabel(str(change)))
            table.setCellWidget(index + 1, 5, QLabel(str(percentage_change)))

        self.widgets["portfolio"]["table"] = table

    def define_price_widgets(self):
        table = QTableWidget()
        table.setRowCount(1)
        table.setColumnCount(5)
        table.setCellWidget(0, 0, QLabel("Id"))
        table.setCellWidget(0, 1, QLabel("Ticker"))
        table.setCellWidget(0, 2, QLabel("Last Price"))
        table.setCellWidget(0, 3, QLabel("Source"))
        table.setCellWidget(0, 4, QLabel("Date"))
        self.widgets["price"]["table"] = table
        self.widgets["price"]["button_return"] = QPushButton("Return")

    def update_price_data(self):
        data = print_open_prices(self.database)
        table = self.widgets["price"]["table"]
        table.setRowCount(len(data) + 1)
        for index, row in enumerate(data):
            table.setCellWidget(index + 1, 0, QLabel(str(row["id"])))
            table.setCellWidget(index + 1, 1, QLabel(str(row["ticker"])))
            table.setCellWidget(index + 1, 2, QLabel(str(row["last_price"])))
            table.setCellWidget(index + 1, 3, QLabel(str(row["price_type"])))
            table.setCellWidget(index + 1, 4, QLabel(str(row["date"])))

        self.widgets["price"]["table"] = table

    def define_eod_widgets(self):
        self.widgets["eod"]["title_label"] = QLabel("Render EOD for prices")
        self.widgets["eod"]["instruction_label"] = QLabel("Today's date (Format: YYYY-MM-DD)")
        self.widgets["eod"]["date_input"] = QLineEdit()
        self.widgets["eod"]["button_validate"] = QPushButton("Ok")
        self.widgets["eod"]["button_cancel"] = QPushButton("Cancel")

    def define_credits_widget(self):
        self.widgets["credits"]["credits_label"] = QLabel("Credits:")
        self.widgets["credits"]["luminai"] = QLabel("luminai@gmail.com")
        self.widgets["credits"]["luminai"].setAlignment(Qt.AlignCenter)
        self.widgets["credits"]["yukeien"] = QLabel("yukeiensan@gmail.com")
        self.widgets["credits"]["yukeien"].setAlignment(Qt.AlignCenter)
        self.widgets["credits"]["button_return"] = QPushButton("Return")

    def define_button_events(self):
        # Menu
        self.widgets["default"]["trade_button"].clicked.connect(self.trade_layout_action)
        self.widgets["default"]["activity_button"].clicked.connect(self.activity_layout_action)
        self.widgets["default"]["deposit_button"].clicked.connect(self.deposit_layout_action)
        self.widgets["default"]["portfolio_button"].clicked.connect(self.portfolio_layout_action)
        self.widgets["default"]["price_button"].clicked.connect(self.price_layout_action)
        self.widgets["default"]["eod_button"].clicked.connect(self.eod_layout_action)
        self.widgets["default"]["credits_button"].clicked.connect(self.credits_layout_action)

        # Trade
        self.widgets["trade"]["button_validate"].clicked.connect(self.validate_trade_action)
        self.widgets["trade"]["button_cancel"].clicked.connect(self.cancel_trade_action)

        # Activity
        self.widgets["activity"]["button_return"].clicked.connect(self.back_activity_action)

        # Deposit
        self.widgets["deposit"]["button_validate"].clicked.connect(self.validate_deposit_action)
        self.widgets["deposit"]["button_cancel"].clicked.connect(self.cancel_deposit_action)

        # Portfolio
        self.widgets["portfolio"]["button_return"].clicked.connect(self.back_portfolio_action)

        # Price
        self.widgets["price"]["button_return"].clicked.connect(self.back_price_action)

        # EOD
        self.widgets["eod"]["button_validate"].clicked.connect(self.validate_eod_action)
        self.widgets["eod"]["button_cancel"].clicked.connect(self.cancel_eod_action)

        # Credits
        self.widgets["credits"]["button_return"].clicked.connect(self.back_credits_action)

    def link_widgets_to_layout(self):
        for label, widgets in self.widgets.items():
            for widget_label, widget in widgets.items():
                self.layout.addWidget(widget)
                if label != "header":
                    widget.hide()

    def toggle_layout(self):
        if self.old_toggle:
            for label, widget in self.widgets[self.old_toggle].items():
                widget.hide()
                if self.old_toggle == "credits":
                    for header_label, header_widget in self.widgets["header"].items():
                        header_widget.show()

        if self.toggle:
            for label, widget in self.widgets[self.toggle].items():
                widget.show()
                if self.toggle == "credits":
                    for header_label, header_widget in self.widgets["header"].items():
                        header_widget.hide()

    def update_infos(self):
        self.portfolio_value = get_portfolio_value(self.database)
        self.cash_balance_value = get_cash_balance(self.database)
        middle = 0
        if self.cash_balance_value < 0:
            self.debt_value = self.cash_balance_value * -1
            self.cash_balance_value = 0
        else:
            self.debt_value = 0
        self.widgets["header"]["portfolio_value_label"]\
            .setText("Market value of securities: $" + str(self.portfolio_value))
        self.widgets["header"]["cash_balance_value_label"].setText("Cash balance: $" + str(self.cash_balance_value))
        self.widgets["header"]["debt_label"].setText("Current debt: $" + str(self.debt_value))
        if self.portfolio_value != 0:
            self.debt_percentage = self.debt_value / self.portfolio_value
            middle = self.portfolio_value / 2
        else:
            self.debt_percentage = 0

        if self.debt_percentage > 0.5:
            self.error_dialog.showMessage("Account flagged - debt over 50% - Current debt: "
                                          + str(round(self.debt_percentage * 100, 2)) + "%")

            file = open("flag.txt", "w+")
            file.write("//!\\\\ Account flagged - debt over 50% - Current debt: "
                       + str(round(self.debt_percentage * 100, 2)) + "% //!\\\\\n")
            file.write("Sell for $" + str(self.debt_value - middle) + " to get unflagged")

        self.widgets["header"]["debt_to_go"].setText("Amount to get unflagged: " + str(self.debt_value - middle))

    def switch_layout(self, layout):
        self.old_toggle = self.toggle
        self.toggle = layout
        self.update_infos()
        self.toggle_layout()

    def reset_trade_layout(self):
        self.widgets["trade"]["action_list"].setCurrentIndex(0)
        self.widgets["trade"]["share_input"].setText("")
        self.widgets["trade"]["share_amount_input"].setValue(0)
        self.widgets["trade"]["share_price_input"].setValue(0.00)
        self.switch_layout("default")

    def eod_database_update(self, date_for_eod):
        arr = get_list_of_open_positions_for_eod(self.database)
        for item in arr:
            ticker = item['ticker']
            price, _type, _, _id = get_last_price_type_date_id_by_ticker(self.database, ticker)
            set_eod_for_ticker(self.database, ticker, price, date_for_eod)

    # Main Menu Actions
    @Slot()
    def trade_layout_action(self):
        self.switch_layout("trade")

    @Slot()
    def activity_layout_action(self):
        self.update_activity_data()
        self.switch_layout("activity")

    @Slot()
    def deposit_layout_action(self):
        self.switch_layout("deposit")

    @Slot()
    def portfolio_layout_action(self):
        self.update_portfolio_data()
        self.switch_layout("portfolio")

    @Slot()
    def price_layout_action(self):
        self.update_price_data()
        self.switch_layout("price")

    @Slot()
    def eod_layout_action(self):
        self.switch_layout("eod")

    @Slot()
    def credits_layout_action(self):
        self.switch_layout("credits")

    # Trade Actions
    @Slot()
    def validate_trade_action(self):
        ticker = self.widgets["trade"]["share_input"].text()
        shares = self.widgets["trade"]["share_amount_input"].value()
        price = self.widgets["trade"]["share_price_input"].value()
        action_list = self.widgets["trade"]["action_list"]
        action = action_list.currentText()
        if action == "Buy":
            action = ActionType.BUY
        elif action == "Sell":
            action = ActionType.SELL
        status = transaction(self.database, self.config, ticker, shares, price, action)
        if not status:
            self.error_dialog.showMessage("Error happened during trade")
        self.update_infos()
        self.reset_trade_layout()

    @Slot()
    def cancel_trade_action(self):
        self.reset_trade_layout()

    # Activity Actions
    @Slot()
    def back_activity_action(self):
        self.switch_layout("default")

    # Deposit Actions
    @Slot()
    def validate_deposit_action(self):
        deposit(self.database, self.widgets["deposit"]["input"].value())
        self.widgets["deposit"]["input"].setValue(0.00)
        self.update_infos()
        self.switch_layout("default")

    @Slot()
    def cancel_deposit_action(self):
        self.widgets["deposit"]["input"].setValue(0.00)
        self.switch_layout("default")

    # Portfolio Actions
    @Slot()
    def back_portfolio_action(self):
        self.switch_layout("default")

    # Price Actions
    @Slot()
    def back_price_action(self):
        self.switch_layout("default")

    # EOD Actions
    @Slot()
    def validate_eod_action(self):
        open_prices = print_open_prices(self.database)
        try:
            date_for_eod = datetime.strptime(self.widgets["eod"]["date_input"].text(), "%Y-%m-%d")
            eod_date_validation(open_prices, date_for_eod, self.widgets["eod"]["date_input"].text())
            self.eod_database_update(date_for_eod)
            self.widgets["eod"]["date_input"].setText("")
            self.update_price_data()
            self.switch_layout("price")
        except Exception as err:
            self.error_dialog.showMessage(str(err))
            self.widgets["eod"]["date_input"].setText("")
            self.switch_layout("default")

    @Slot()
    def cancel_eod_action(self):
        self.widgets["eod"]["date_input"].setText("")
        self.switch_layout("default")

    # Credits Actions
    @Slot()
    def back_credits_action(self):
        self.switch_layout("default")
